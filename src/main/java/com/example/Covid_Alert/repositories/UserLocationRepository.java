package com.example.Covid_Alert.repositories;

import com.example.Covid_Alert.models.User_Locations;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserLocationRepository extends JpaRepository<User_Locations,Long> {
}
