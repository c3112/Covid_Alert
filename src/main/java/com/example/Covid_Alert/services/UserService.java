package com.example.Covid_Alert.services;

import com.example.Covid_Alert.dto.UserDTO;
import com.example.Covid_Alert.models.User;
import com.example.Covid_Alert.repositories.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Configurable
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private final KeycloakService keycloakService = new KeycloakService();

    public User createUserFromDTO(UserDTO registerUser) {
        User user = new User();
        user.setUsername(registerUser.getUsername());
        user.setFirst_name(registerUser.getfirstName());
        user.setLast_name(registerUser.getlastName());
        user.setEmail(registerUser.getEmail());
        user.setPassword("00000000");
        user.setEnabled(true);
        return user;
    }

    public ResponseEntity deleteUser(String username) {
        if (username != null) {
            userRepository.deleteByUsername(username);
            return null; //TODO
        } else {
            return new ResponseEntity(403, HttpStatus.valueOf("Missing parameters"));
        }
        //TODO : logout user
        //Delete from DB
        //Delete from Keycloak
    }


}
