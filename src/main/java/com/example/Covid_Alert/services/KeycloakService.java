package com.example.Covid_Alert.services;

import com.example.Covid_Alert.KeycloakConfig;
import com.example.Covid_Alert.models.User;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class KeycloakService {

    private final Keycloak keycloak = KeycloakConfig.getInstance();

    private final RestTemplate restTemplate = new RestService(new RestTemplateBuilder()).getRestTemplate();


    public ResponseEntity loginKeycloak(User userData) {
        try {
            String keycloakLoginUrl = KeycloakConfig.serverUrl + "/realms/" + KeycloakConfig.realm + "/protocol/openid-connect/token";

            MultiValueMap<String, String> requestBodyParams = new LinkedMultiValueMap<>();

            requestBodyParams.add("grant_type", "password");
            requestBodyParams.add("client_id", KeycloakConfig.clientId);
            requestBodyParams.add("client_secret", KeycloakConfig.clientSecret);
            requestBodyParams.add("username", userData.getUsername());
            requestBodyParams.add("password", userData.getPassword());

            HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(requestBodyParams, null);
            ResponseEntity response = this.restTemplate.postForEntity(keycloakLoginUrl, httpEntity, Object.class);

            if (response.getStatusCode() == HttpStatus.OK) {

                System.out.println("========== RESPONSE" + response);
                //UserRepresentation userRepresentation = keycloak.realm(KeycloakConfig.realm).users().search(userData.getUsername()).get(0);
                return new ResponseEntity<>(response, response.getStatusCode());
            } else {
                System.out.println(" =========================== ERR");
            }


        } catch (Exception e) {
            System.err.println("Erreur" + e);
        }
        return null;
    }

    public ResponseEntity registerKeycloak(User userData) {
        UserRepresentation user = new UserRepresentation();

        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(userData.getPassword());

        user.setUsername(userData.getEmail());
        user.setFirstName(userData.getFirst_name());
        user.setLastName(userData.getLast_name());
        user.setCredentials(Collections.singletonList(credentialRepresentation));
        user.setEmail(userData.getEmail());
        user.setEnabled(true);

        keycloak.realm(KeycloakConfig.realm).users().create(user);

        String newUserId = keycloak.realm(KeycloakConfig.realm).users().search(user.getUsername()).get(0).getId();

        RoleRepresentation roleRepresentation = keycloak.realm(KeycloakConfig.realm).roles().get("ROLE_USER").toRepresentation();
        keycloak.realm(KeycloakConfig.realm).users().get(newUserId).roles().realmLevel().add(Collections.singletonList(roleRepresentation));

        keycloak.realm(KeycloakConfig.realm).users().get(newUserId).sendVerifyEmail();

        return null;
    }
}
