package com.example.Covid_Alert.controllers;

import com.example.Covid_Alert.dto.LocationDTO;
import com.example.Covid_Alert.models.Location;
import com.example.Covid_Alert.models.User_Locations;
import com.example.Covid_Alert.repositories.LocationRepository;
import com.example.Covid_Alert.repositories.UserLocationRepository;
import com.example.Covid_Alert.util.OnCreateLocationEvent;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class GetLocationController {
    @Autowired
    LocationRepository locationRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    UserLocationRepository userLocationRepository;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @PostMapping("/addLocations")
    public Location saveLocation(@RequestBody LocationDTO locationDTO) {

        Date date = new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);

        Location location = new Location();
        location.setLocation_date(ts);
        location.setLatitude(locationDTO.getLatitude());
        location.setLongitude(locationDTO.getLongitude());

        kafkaTemplate.send("topic-1", getJsonUserLocation(location, locationDTO.getUsername()));
        return location;
    }

    public String getJsonUserLocation(Location location, String username) {
        Map userlocation = new HashMap();
        userlocation.put("username", username);
        userlocation.put("latitude", location.getLatitude());
        userlocation.put("longitude", location.getLongitude());
        userlocation.put("timestamp", location.getLocation_date());
        Gson gson = new Gson();
        String json = gson.toJson(userlocation);
        return json;
    }
}
