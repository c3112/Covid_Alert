package com.example.Covid_Alert.controllers;

import com.example.Covid_Alert.dto.UserDTO;
import com.example.Covid_Alert.models.User;
import com.example.Covid_Alert.repositories.UserRepository;
import com.example.Covid_Alert.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/users")
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    private final UserService userService = new UserService();

    @GetMapping
    public List<User> getUsers() {
        return userRepository.findAll();
    }


    @GetMapping(value = "/{username}")
    public User getUserByUsername(@PathVariable String username) {
        return userRepository.getByUsername(username);
    }

    @PostMapping("/createUser")
    public User createUser(@RequestBody UserDTO user) {
        return userRepository.saveAndFlush(userService.createUserFromDTO(user));
    }

    @PostMapping("/updateUser")
    public User updateUser(@RequestBody UserDTO user) {
        User existingUser = userRepository.getByUsername(user.getUsername());
        User newUser = userService.createUserFromDTO(user);
        BeanUtils.copyProperties(newUser, existingUser, "user_id");
        //TODO update in keycloak
        return userRepository.saveAndFlush(newUser);
    }

    @DeleteMapping("/deleteUser")
    public ResponseEntity deleteUser(@RequestBody String username) {
        return userService.deleteUser(username);
    }
}
