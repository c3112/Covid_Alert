package com.example.Covid_Alert;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeycloakConfig {

    static Keycloak keycloak = null;

    public final static String serverUrl = System.getenv("KEYCLOAK_SERVER_URL");
    public final static String realm = System.getenv("KEYCLOAK_REALM");
    public final static String clientId = System.getenv("KEYCLOAK_CLIENT_ID");
    public final static String username = System.getenv("KEYCLOAK_USERNAME");
    public final static String password = System.getenv("KEYCLOAK_PASSWORD");
    public final static String clientSecret = System.getenv("KEYCLOAK_CLIENT_SECRET");

    public static Keycloak getInstance() {
        if (keycloak == null) {
            keycloak = KeycloakBuilder.builder()
                    .serverUrl(serverUrl)
                    .realm(realm)
                    .username(username)
                    .password(password)
                    .clientId(clientId)
                    .clientSecret(clientSecret)
                    .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                    .build();
        }
        return keycloak;
    }
}

